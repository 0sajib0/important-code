UPDATE
-- SELECT t1.id 
-- FROM
tmp_eboighar_books t1,
(
	SELECT s.*
	FROM
	(
		SELECT j0.id,j0.title_ban,j0.final_category,j1.Id CategoryID,j0.sub_category,j2.Id SubCatId
		FROM
		(
			SELECT * FROM tmp_eboighar_books
		) j0
		LEFT OUTER JOIN book_category j1 ON(j0.final_category=j1.Name)
		LEFT OUTER JOIN book_sub_category j2 ON(j0.sub_category=j2.Name AND j2.CatId=j1.Id)
		ORDER BY j0.id
	) s
	WHERE s.SubCatID IS NULL
) t2
SET t1.upload_sts = 0
WHERE t1.id = t2.id