--SELECT * FROM `customer` WHERE `CustomerId` = '1905212'; -- 38952
--SELECT * FROM `customer_credit_facility` WHERE `CustomerId` = '38952';

--INSERT INTO segment (CODE,NAME)
--SELECT segment,segment FROM v1_v2_segment_migrate GROUP BY segment;

--UPDATE v1_v2_segment_migrate SET seg_id=9 WHERE segment='CNIB';
--UPDATE v1_v2_segment_migrate SET seg_id=10 WHERE segment='CONSUMER';
--UPDATE v1_v2_segment_migrate SET seg_id=11 WHERE segment='MSME-SMALL';
--UPDATE v1_v2_segment_migrate SET seg_id=12 WHERE segment='STAFF';

UPDATE v1_v2_segment_migrate t1, customer t2 
SET t1.cust_id_pk=t2.Id WHERE t1.cus_id=t2.CustomerId AND t2.State=1;


----start
ALTER TABLE `edoc_pbl`.`cad_comment` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`customer_bank_guarantee` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`customer_cash_collateral` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`customer_credit_facility` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `approval_authority`; 
ALTER TABLE `edoc_pbl`.`customer_documentation` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `Floating_Assets_55_56`;
ALTER TABLE `edoc_pbl`.`customer_insurance_policy` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `PolicyValue`; 
ALTER TABLE `edoc_pbl`.`customer_locked` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`customer_locked_histry` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`customer_others_document_cheque` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`doc_manager_tick` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`title_level_access` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`v3_dia_deed` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`v3_documents_file` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`v3_lien_share_documents` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;
ALTER TABLE `edoc_pbl`.`v3_orginal_mortgage_ddd` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;

ALTER TABLE `edoc_pbl`.`v3_orginal_title_ddd` ADD COLUMN `SegmentIdOld` INT(2) NOT NULL AFTER `State`;

---end then start
UPDATE cad_comment t1,cad_comment t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_bank_guarantee t1,customer_bank_guarantee t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_cash_collateral t1,customer_cash_collateral t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_credit_facility t1,customer_credit_facility t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_documentation t1,customer_documentation t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_insurance_policy t1,customer_insurance_policy t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_locked t1,customer_locked t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_locked_histry t1,customer_locked_histry t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE customer_others_document_cheque t1,customer_others_document_cheque t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE doc_manager_tick t1,doc_manager_tick t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE title_level_access t1,title_level_access t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE v3_dia_deed t1,v3_dia_deed t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE v3_documents_file t1,v3_documents_file t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE v3_lien_share_documents t1,v3_lien_share_documents t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE v3_orginal_mortgage_ddd t1,v3_orginal_mortgage_ddd t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;
UPDATE v3_orginal_title_ddd t1,v3_orginal_title_ddd t2 SET t1.SegmentIdOld=t2.SegmentId WHERE t1.Id=t2.Id;

---- End Then start
UPDATE cad_comment t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_bank_guarantee t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_cash_collateral t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_credit_facility t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;
UPDATE customer_documentation t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_insurance_policy t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_locked t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_locked_histry t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE customer_others_document_cheque t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE doc_manager_tick t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE title_level_access t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE v3_dia_deed t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE v3_documents_file t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE v3_lien_share_documents t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE v3_orginal_mortgage_ddd t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;

UPDATE v3_orginal_title_ddd t1,
(
	SELECT seg_id,cust_id_pk FROM v1_v2_segment_migrate
) t2
SET t1.SegmentId=t2.seg_id
WHERE t1.CustomerId = t2.cust_id_pk;


--cad_comment
--customer_bank_guarantee
--customer_cash_collateral
--customer_credit_facility
--customer_documentation
--customer_insurance_policy
--customer_locked
--customer_locked_histry
--customer_others_document_cheque
--doc_manager_tick
--title_level_access
--v3_dia_deed
--v3_documents_file
--v3_lien_share_documents
--v3_orginal_mortgage_ddd
--v3_orginal_title_ddd