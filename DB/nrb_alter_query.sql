Parameter->document Title->Search  'Mortgage Document - Original Mortgage Deed' ->edit-> change property type LIST to Multiple select
Parameter->document Title->Search  'Mortgage Document - Original Mortgage Deed' ->edit-> change Mortgage Property Serial No Multiple Select LIST





ALTER TABLE `edoc_nrb`.`app_cust_odp_ref_id` CHANGE `director_of_bank_id` `director_of_bank_id` TEXT NOT NULL; 
ALTER TABLE `edoc_nrb`.`app_cust_odp_info` CHANGE `director_of_bank` `director_of_bank` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL; 
ALTER TABLE `edoc_nrb`.`app_cust_odp_ref_id` CHANGE `directr_of_nbfi_id` `directr_of_nbfi_id` TEXT NOT NULL; 
ALTER TABLE `edoc_nrb`.`app_cust_odp_info` CHANGE `directr_of_nbfi` `directr_of_nbfi` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `edoc_nrb`.`app_cust_odp_info` ADD COLUMN `odp_cif` VARCHAR(50) NOT NULL AFTER `owner_cib_sub_code`; 



ALTER TABLE `edoc_nrb`.`ch_audited_financial` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_bia_deed` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE `edoc_nrb`.`ch_boiler_certificate` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_bonded_warehouse_license` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_busines_auth_doe` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_car_attribute` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_charge_creation_with_rjsc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_erc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_factory_license` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_fire_and_cd_certificate` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_fourth_party` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_guarantee_document` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_instrument_under_lien` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_insurance_policy` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_irc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_lc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_lease_agreement` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_membership_certificate` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_municipal_tax_paid_receipt` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_ngo_registration_certificate` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_original_lease_deed` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_original_mortgage_deed` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_original_title_deed` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_other_instrument_under_lien` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_p_general` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_post_dated_cheque` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_registered_igpa` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_rent_paid_receipt` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_schedule_of_property` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_survey_of_property` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_tax_clearance_certificate` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_tin` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_trade_license_` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_tri_partite_agreement` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_vat` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `edoc_nrb`.`ch_work_order_details` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL;



ALTER TABLE `edoc_nrb`.`docs_seg_1` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `edoc_nrb`.`docs_seg_2` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `edoc_nrb`.`docs_seg_3` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `edoc_nrb`.`docs_seg_4` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Deferral','Waiver','Inadequacy','Pending','Photocopy','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;


UPDATE `edoc_nrb`.`par_docs_title` SET `has_child_sts` = '1' , `child_table_name` = 'ch_cs_khatian' , `file_upload_sts` = '0' , `tagable_sts` = '0' , `cforward_sts` = '0' WHERE `id` = '110'; 
UPDATE `edoc_nrb`.`par_docs_title` SET `has_child_sts` = '1' , `child_table_name` = 'ch_sa_khatian' , `file_upload_sts` = '0' , `tagable_sts` = '0' , `cforward_sts` = '0' WHERE `id` = '111';
UPDATE `edoc_nrb`.`par_docs_title` SET `has_child_sts` = '1' , `child_table_name` = 'ch_rs_khatian' , `file_upload_sts` = '0' , `tagable_sts` = '0' , `cforward_sts` = '0' WHERE `id` = '112';


insert into `child_table_list` (`name`, `child_table_name`, `docs_title_id`, `child_tagable_sts`, `child_cforward_sts`, `table_size`, `sts`) values('CS khatian','ch_cs_khatian','110','0','0','90','1');
insert into `child_table_list` (`name`, `child_table_name`, `docs_title_id`, `child_tagable_sts`, `child_cforward_sts`, `table_size`, `sts`) values('SA khatian','ch_sa_khatian','111','0','0','90','1');
insert into `child_table_list` (`name`, `child_table_name`, `docs_title_id`, `child_tagable_sts`, `child_cforward_sts`, `table_size`, `sts`) values('RS khatian','ch_rs_khatian','112','0','0','90','1');


INSERT INTO `child_table_fields` (`child_table_name`, `caption_name`, `field_name`, `input_type`, `ref_table_name`, `mandatory_sts`, `unique_sts`, `filter_sts`, `orders`, `field_size`, `is_editable`) VALUES('ch_cs_khatian','Mortgage Property Serial No','field_1','List','ref_property_serial_no','1','0','1','1','30','0');
INSERT INTO `child_table_fields` (`child_table_name`, `caption_name`, `field_name`, `input_type`, `ref_table_name`, `mandatory_sts`, `unique_sts`, `filter_sts`, `orders`, `field_size`, `is_editable`) VALUES('ch_cs_khatian','File','field_2','File','0','0','0','0','2','20','0');
INSERT INTO `child_table_fields` (`child_table_name`, `caption_name`, `field_name`, `input_type`, `ref_table_name`, `mandatory_sts`, `unique_sts`, `filter_sts`, `orders`, `field_size`, `is_editable`) VALUES('ch_sa_khatian','Mortgage Property Serial No','field_1','List','ref_property_serial_no','1','0','1','1','30','0');
INSERT INTO `child_table_fields` (`child_table_name`, `caption_name`, `field_name`, `input_type`, `ref_table_name`, `mandatory_sts`, `unique_sts`, `filter_sts`, `orders`, `field_size`, `is_editable`) VALUES('ch_sa_khatian','File','field_2','File','0','0','0','0','2','20','0');
INSERT INTO `child_table_fields` (`child_table_name`, `caption_name`, `field_name`, `input_type`, `ref_table_name`, `mandatory_sts`, `unique_sts`, `filter_sts`, `orders`, `field_size`, `is_editable`) VALUES('ch_rs_khatian','Mortgage Property Serial No','field_1','List','ref_property_serial_no','1','0','1','1','30','0');
INSERT INTO `child_table_fields` (`child_table_name`, `caption_name`, `field_name`, `input_type`, `ref_table_name`, `mandatory_sts`, `unique_sts`, `filter_sts`, `orders`, `field_size`, `is_editable`) VALUES('ch_rs_khatian','File','field_2','File','0','0','0','0','2','20','0');




DROP TABLE IF EXISTS `ch_cs_khatian`;

CREATE TABLE `ch_cs_khatian` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cust_id` bigint(20) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `seg_id` int(2) DEFAULT NULL,
  `app_ref_no` varchar(150) NOT NULL,
  `title_id` bigint(20) DEFAULT NULL,
  `docs_id` bigint(20) DEFAULT NULL,
  `doc_sts` enum('Obtain','Deferral','Inadequacy','Waiver','Pending','N/A') DEFAULT NULL,
  `ch_pending_dt` date NOT NULL,
  `ch_facility_line_ids` text NOT NULL,
  `ch_shared_with` text NOT NULL,
  `ch_shared_from` bigint(20) NOT NULL DEFAULT '0',
  `ch_shared_id` bigint(20) DEFAULT '0',
  `ch_cf_with` bigint(20) NOT NULL DEFAULT '0',
  `ch_cf_from` bigint(20) NOT NULL DEFAULT '0',
  `safe_out_sts` tinyint(1) NOT NULL DEFAULT '0',
  `safe_out_dt` date DEFAULT NULL,
  `safe_out_by` int(11) DEFAULT NULL,
  `safe_out_ref` varchar(20) DEFAULT NULL,
  `safe_out_comment` text,
  `field_1` varchar(255) NOT NULL,
  `field_2` text,
  `field_2_file_type` int(2) NOT NULL DEFAULT '0',
  `field_2_amount` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `field_2_risk_type` int(2) NOT NULL DEFAULT '0',
  `field_2_date_doc` date NOT NULL,
  `e_by` int(5) DEFAULT NULL,
  `e_dt` datetime DEFAULT NULL,
  `u_by` int(5) DEFAULT NULL,
  `u_dt` datetime DEFAULT NULL,
  `d_by` int(5) DEFAULT NULL,
  `d_dt` datetime DEFAULT NULL,
  `sts` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `ch_rs_khatian` */

DROP TABLE IF EXISTS `ch_rs_khatian`;

CREATE TABLE `ch_rs_khatian` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cust_id` bigint(20) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `seg_id` int(2) DEFAULT NULL,
  `app_ref_no` varchar(150) NOT NULL,
  `title_id` bigint(20) DEFAULT NULL,
  `docs_id` bigint(20) DEFAULT NULL,
  `doc_sts` enum('Obtain','Deferral','Inadequacy','Waiver','Pending','N/A') DEFAULT NULL,
  `ch_pending_dt` date NOT NULL,
  `ch_facility_line_ids` text NOT NULL,
  `ch_shared_with` text NOT NULL,
  `ch_shared_from` bigint(20) NOT NULL DEFAULT '0',
  `ch_shared_id` bigint(20) DEFAULT '0',
  `ch_cf_with` bigint(20) NOT NULL DEFAULT '0',
  `ch_cf_from` bigint(20) NOT NULL DEFAULT '0',
  `safe_out_sts` tinyint(1) NOT NULL DEFAULT '0',
  `safe_out_dt` date DEFAULT NULL,
  `safe_out_by` int(11) DEFAULT NULL,
  `safe_out_ref` varchar(20) DEFAULT NULL,
  `safe_out_comment` text,
  `field_1` varchar(255) NOT NULL,
  `field_2` text,
  `field_2_file_type` int(2) NOT NULL DEFAULT '0',
  `field_2_amount` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `field_2_risk_type` int(2) NOT NULL DEFAULT '0',
  `field_2_date_doc` date NOT NULL,
  `e_by` int(5) DEFAULT NULL,
  `e_dt` datetime DEFAULT NULL,
  `u_by` int(5) DEFAULT NULL,
  `u_dt` datetime DEFAULT NULL,
  `d_by` int(5) DEFAULT NULL,
  `d_dt` datetime DEFAULT NULL,
  `sts` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `ch_sa_khatian` */

DROP TABLE IF EXISTS `ch_sa_khatian`;

CREATE TABLE `ch_sa_khatian` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cust_id` bigint(20) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `seg_id` int(2) DEFAULT NULL,
  `app_ref_no` varchar(150) NOT NULL,
  `title_id` bigint(20) DEFAULT NULL,
  `docs_id` bigint(20) DEFAULT NULL,
  `doc_sts` enum('Obtain','Deferral','Inadequacy','Waiver','Pending','N/A') DEFAULT NULL,
  `ch_pending_dt` date NOT NULL,
  `ch_facility_line_ids` text NOT NULL,
  `ch_shared_with` text NOT NULL,
  `ch_shared_from` bigint(20) NOT NULL DEFAULT '0',
  `ch_shared_id` bigint(20) DEFAULT '0',
  `ch_cf_with` bigint(20) NOT NULL DEFAULT '0',
  `ch_cf_from` bigint(20) NOT NULL DEFAULT '0',
  `safe_out_sts` tinyint(1) NOT NULL DEFAULT '0',
  `safe_out_dt` date DEFAULT NULL,
  `safe_out_by` int(11) DEFAULT NULL,
  `safe_out_ref` varchar(20) DEFAULT NULL,
  `safe_out_comment` text,
  `field_1` varchar(255) NOT NULL,
  `field_2` text,
  `field_2_file_type` int(2) NOT NULL DEFAULT '0',
  `field_2_amount` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `field_2_risk_type` int(2) NOT NULL DEFAULT '0',
  `field_2_date_doc` date NOT NULL,
  `e_by` int(5) DEFAULT NULL,
  `e_dt` datetime DEFAULT NULL,
  `u_by` int(5) DEFAULT NULL,
  `u_dt` datetime DEFAULT NULL,
  `d_by` int(5) DEFAULT NULL,
  `d_dt` datetime DEFAULT NULL,
  `sts` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


ALTER TABLE `edoc_nrb`.`app_loan_facility_line` ADD COLUMN `penal_int_rate` DECIMAL(16,2) NOT NULL AFTER `int_rate_dt`;
ALTER TABLE `edoc_nrb`.`app_loan_facility_line_history` ADD COLUMN `penal_int_rate` DECIMAL(16,2) NOT NULL AFTER `int_rate_dt`;

INSERT INTO `edoc_nrb`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('7', '40', 'verify', 'Verify Customer Rating', 'cust_rating/view');

ALTER TABLE `edoc_nrb`.`app_guarantee_ref` CHANGE `director_of_bank` `director_of_bank` TEXT NOT NULL, CHANGE `directr_of_nbfi` `directr_of_nbfi` TEXT NOT NULL, CHANGE `profession_id` `profession_id` INT(11) NULL;
