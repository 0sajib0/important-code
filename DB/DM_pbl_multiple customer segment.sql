-- Customers having multiple segments and multiple documents
-----------------------------------------------------------------
SELECT c.CustomerId, c.Name, t1.segment_name
FROM customer c
INNER JOIN (
	SELECT d.CustomerId, GROUP_CONCAT(DISTINCT s.Name) AS segment_name -- c.SegmentId
	FROM customer_documentation d
	INNER JOIN (
		SELECT CustomerId
		FROM customer_documentation
		WHERE State=1
		GROUP BY CustomerId
		HAVING COUNT(DISTINCT SegmentId)>1
	) t ON t.CustomerId = d.CustomerId
	JOIN segment s ON s.Id = d.SegmentId
	GROUP BY d.CustomerId
) t1 ON t1.CustomerId = c.Id AND c.State=1
LIMIT 999999
;


SELECT * FROM customer c
WHERE c.CustomerId = '759120'
;
-- 10550
SELECT * FROM customer_documentation d 
WHERE d.CustomerId = 10550
;

-- ==========================================================================
-- customers with segments
select c.`CustomerId`,c.`Name`, t.segment_name
FROM `customer` c
left join 
(
SELECT f. `CustomerId`, group_concat(DISTINCT s.Name) as segment_name 
FROM `customer_credit_facility` f
left join `segment` s on s.Id=f.`SegmentId`
group by f.CustomerId
) t on t.CustomerId = c.Id
WHERE c.State <> 0
limit 999999
;