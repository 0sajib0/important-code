UPDATE app_loan_approval a,
(SELECT s1.*,j2.loan_approval_ref_no 
	FROM
	(
		SELECT j0.app_ref_no, j1.app_id 
		FROM 
		title_level_access j0
		INNER JOIN docs_seg_2 j1 ON(j0.customer_id=j1.cust_id AND j0.app_ref_no=j1.app_ref_no AND j0.segment_id=j1.prod_seg_id AND j1.sts=1)
		WHERE j0.sts=1 GROUP BY j0.id 
	) s1
	INNER JOIN app_loan_approval j2 ON(j2.id=s1.app_id)
) s2

SET a.loan_approval_ref_no = s2.app_ref_no

WHERE a.sts=1 AND a.id=s2.app_id AND a.loan_approval_ref_no!=s2.app_ref_no

