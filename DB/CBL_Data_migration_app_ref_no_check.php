SELECT aa.cust_id,GROUP_CONCAT(aa.loan_approval_ref_no),   s1.*
FROM
(

	
	SELECT a.cust_id,a.loan_approval_ref_no,a.segment_id ,
	s.customer_id,s.app_ref_no,s.segment_id AS s_segment_id
	FROM title_level_access s
	LEFT OUTER JOIN app_loan_approval a  ON (a.cust_id=s.customer_id AND a.segment_id=s.segment_id AND a.loan_approval_ref_no=s.app_ref_no AND a.sts=1)
	WHERE  s.sts=1 AND a.cust_id IS NULL
	ORDER BY a.cust_id
	
) s1
LEFT OUTER JOIN app_loan_approval aa ON (aa.cust_id=s1.customer_id AND aa.segment_id=s1.s_segment_id AND aa.sts=1)
GROUP BY s1.customer_id,s1.segment_id,s1.app_ref_no

INSERT INTO `edoc_cbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`, `s_order`) VALUES ('4', '21', 'view', 'Customer Eligible Security Report', 'doc_report/eligible_security', '15'); 


INSERT INTO `edoc_cbl_v2`.`sys_links` (`name`) VALUES ('Customer Un-Eligible Security Report');

UPDATE `edoc_cbl_v2`.`sys_links` SET `sys_link_group_id` = '4' , `sys_link_cat_id` = '21' , `operations` = 'view' , `url_prefix` = 'doc_report/un_eligible_security' , `s_order` = '16' WHERE `name` = 'Customer Un-Eligible Security Report'; 

