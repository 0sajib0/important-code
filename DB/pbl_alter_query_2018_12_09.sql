UPDATE ref_nature_of_security SET sts=0 WHERE id IN (6,7,10,20);

ALTER TABLE `edoc_pbl_v2`.`app_loan_facility_line` ADD COLUMN `earmark_sts` TINYINT(1) DEFAULT 0 NOT NULL AFTER `guarantor_nature`;
ALTER TABLE `edoc_pbl_v2`.`app_loan_facility_line_history` ADD COLUMN `earmark_sts` TINYINT(1) DEFAULT 0 NOT NULL AFTER `guarantor_nature`;