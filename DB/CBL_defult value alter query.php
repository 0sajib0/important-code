ALTER TABLE `edoc_cbl_v2`.`child_table_fields` ADD COLUMN `ref_default_value` VARCHAR(50) NOT NULL AFTER `ref_table_name`;


UPDATE child_table_fields SET ref_default_value='1' WHERE ref_table_name='ref_doc_category';

UPDATE child_table_fields SET ref_default_value='8' WHERE ref_table_name='ref_bank_and_nbfi';

UPDATE child_table_fields SET ref_default_value='1' WHERE ref_table_name='ref_execution_mode';


UPDATE child_table_fields SET ref_default_value='1' WHERE ref_table_name='ref_parties';

UPDATE child_table_fields SET ref_default_value='1' WHERE ref_table_name='ref_ownership';
UPDATE child_table_fields SET ref_default_value='8' WHERE ref_table_name='ref_bank';

UPDATE child_table_fields SET ref_default_value='27' WHERE ref_table_name='ref_nature_of_assets_insured';
