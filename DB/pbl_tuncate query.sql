
-- loan 

TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_combine_facility_name`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_loan_facility_line_ref`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_loan_facility_line_history`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_loan_facility_line`;



-- security

TRUNCATE TABLE `edoc_pbl_v2_dm`.`csi_other_noneligible_security`;
TRUNCATE TABLE `edoc_pbl_v2_dm`.`csi_share_issuer`;
TRUNCATE TABLE `edoc_pbl_v2_dm`.`csi_land_building_flat_gold`;
TRUNCATE TABLE `edoc_pbl_v2_dm`.`csi_cc_equivalant`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`csi_gen_charge_pari_passu_security`; 


-- customer 

TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_customer`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_customer_details`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_customer_ref_id`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_cust_reltd_code`; 
TRUNCATE TABLE `edoc_pbl_v2_dm`.`app_cust_reltd_code_ref_id`;