USE `edoc_pbl_v2`;

/*Table structure for table `ref_customer_role_of_comp` */

DROP TABLE IF EXISTS `ref_customer_role_of_comp`;

CREATE TABLE `ref_customer_role_of_comp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_in_institution` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sts` tinyint(2) DEFAULT '1',
  `e_by` int(11) DEFAULT NULL,
  `e_dt` datetime DEFAULT NULL,
  `u_by` int(11) DEFAULT NULL,
  `u_dt` datetime DEFAULT NULL,
  `d_by` int(11) DEFAULT NULL,
  `d_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ref_customer_role_of_comp` */

insert  into `ref_customer_role_of_comp`(`id`,`name`,`code`,`role_in_institution`,`sts`,`e_by`,`e_dt`,`u_by`,`u_dt`,`d_by`,`d_dt`) values (1,'Chairman','1','Chairman',1,NULL,NULL,1,'2015-09-07 17:08:35',NULL,NULL),(2,'Managing Director','2','Managing Director',1,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Vice Chairman & Sponsor Director','3','Others - Vice Chairman & Sponsor Director\r\n',1,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Sponsor Director','3','Sponsor Director',1,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Vice Chairman & Elected Director','4','Others - Vice Chairman & Elected Director\r\n',1,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Elected Director','4','Elected Director',1,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Nominated Director (by Govt.)','5','Nominated Director (by Govt.)',1,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Nominated Director (by pvt. Institution)','6','Nominated Director (by pvt. Institution)',1,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Partner','7','Partner',1,NULL,NULL,NULL,NULL,NULL,NULL),(10,'Proprietor','8','Owner of Proprietorship\r\n',1,NULL,NULL,NULL,NULL,NULL,NULL),(11,'President','9','Others - President',1,NULL,NULL,NULL,NULL,NULL,NULL),(12,'Vice President','10','Others - Vice President',1,NULL,NULL,NULL,NULL,NULL,NULL),(13,'Trustee','11','Others - Trustee',1,NULL,NULL,NULL,NULL,NULL,NULL),(14,'Member','12','Others - Member',1,NULL,NULL,NULL,NULL,NULL,NULL),(15,'Treasurer','13','Others - Treasurer',1,NULL,NULL,NULL,NULL,NULL,NULL),(16,'Personal(Individual)','14','Others - Personal(Individual)',1,1,'2015-06-10 11:46:20',NULL,NULL,NULL,NULL),(17,'N/A','N/A','Others',1,NULL,NULL,NULL,NULL,NULL,NULL),(18,'Shareholder','15','Shareholder',1,1,'2015-09-08 12:28:54',NULL,NULL,NULL,NULL),(19,'Chairman & MD','16','Chairman & MD',1,NULL,NULL,NULL,NULL,NULL,NULL),(20,'Deputy Managing Director','17','Deputy Managing Director',1,NULL,NULL,NULL,NULL,NULL,NULL),(21,'Director','18','Director',1,NULL,NULL,NULL,NULL,NULL,NULL),(22,'Executive Director','19','Executive Director',1,NULL,NULL,NULL,NULL,NULL,NULL),(23,'Vice Chancellor','3','Vice Chancellor',1,NULL,NULL,NULL,NULL,NULL,NULL),(24,'Principal','3','Principal',1,NULL,NULL,NULL,NULL,NULL,NULL),(25,'Head MASTER/Mistress','3','Head',1,NULL,NULL,NULL,NULL,NULL,NULL),(26,'Registrar','3','Registrar',1,NULL,NULL,NULL,NULL,NULL,NULL);


ALTER TABLE `edoc_pbl_v2`.`app_loan_facility_line_ref` CHANGE `facility_category` `facility_category` VARCHAR(255) NOT NULL;

UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `name` = 'Land/Building/Flat etc.' WHERE `id` = '84';
ALTER TABLE `edoc_pbl_v2`.`csi_cc_equivalant` ADD COLUMN `benificary_name` VARCHAR(150) NULL AFTER `acc_no`; 

 ALTER TABLE `edoc_pbl_v2`.`csi_land_building_flat_gold` ADD COLUMN `location` INT(10) NULL AFTER `collat_description`, ADD COLUMN `land_owner` VARCHAR(150) NULL AFTER `location`;
 ALTER TABLE `edoc_pbl_v2`.`csi_cc_equivalant` ADD COLUMN `shared_with` VARCHAR(255) NULL AFTER `security_shared`;

 INSERT INTO `edoc_pbl_v2`.`sys_link_cat` (`sys_link_group_id`, `name`, `url_prefix`) VALUES ('16', 'Security Preview And Varification', 'cash_and_cash_equivalent/verification');
 UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `s_order` = '5' WHERE `id` = '91'; 
 INSERT INTO `edoc_pbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('16', '91', 'view', 'View securites', 'cash_and_cash_equivalent/view'); 

 ALTER TABLE `edoc_pbl_v2`.`app_customer` ADD COLUMN `security_v_sts` TINYINT(1) DEFAULT 1 NOT NULL AFTER `v_dt`, ADD COLUMN `security_v_by` INT(11) NOT NULL AFTER `security_v_sts`, ADD COLUMN `security_v_dt` DATETIME NOT NULL AFTER `security_v_by`; 

 INSERT INTO `edoc_pbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('16', '91', 'approve', 'Verify security', 'cash_and_cash_equivalent/VIEW');

 ALTER TABLE `edoc_pbl_v2`.`csi_land_building_flat_gold` CHANGE `collat_description` `collat_description` VARCHAR(512) CHARSET utf8 NULL COMMENT 'Collateral Description';

 INSERT INTO `edoc_pbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('16', '81', 'approve', 'Verify cash and cash equivalent security', 'cash_and_cash_equivalent/view');

 ALTER TABLE `edoc_pbl_v2`.`csi_cc_equivalant` ADD COLUMN `v_sts` TINYINT(1) DEFAULT 1 NOT NULL AFTER `sts`, ADD COLUMN `v_dt` DATETIME NULL AFTER `v_sts`, ADD COLUMN `v_by` INT(11) NULL AFTER `v_dt`; 
 
 ALTER TABLE `edoc_pbl_v2`.`csi_land_building_flat_gold` ADD COLUMN `v_sts` TINYINT(1) DEFAULT 1 NOT NULL AFTER `sts`, ADD COLUMN `v_dt` DATETIME NULL AFTER `v_sts`, ADD COLUMN `v_by` INT(11) NULL AFTER `v_dt`; 

 ALTER TABLE `edoc_pbl_v2`.`csi_gen_charge_pari_passu_security` ADD COLUMN `v_sts` TINYINT(1) DEFAULT 1 NOT NULL AFTER `sts`, ADD COLUMN `v_dt` DATETIME NULL AFTER `v_sts`, ADD COLUMN `v_by` INT(11) NULL AFTER `v_dt`; 

ALTER TABLE `edoc_pbl_v2`.`csi_other_noneligible_security` ADD COLUMN `v_sts` TINYINT(1) DEFAULT 1 NOT NULL AFTER `sts`, ADD COLUMN `v_dt` DATETIME NULL AFTER `v_sts`, ADD COLUMN `v_by` INT(11) NULL AFTER `v_dt`; 

 INSERT INTO `edoc_pbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('16', '84', 'approve', 'Verify land,building and flat security', 'land_building_flat_gold/view'); 
INSERT INTO `edoc_pbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('16', '85', 'approve', 'Verify security', 'gen_charge_pari_passu/view'); 
 INSERT INTO `edoc_pbl_v2`.`sys_links` (`sys_link_group_id`, `sys_link_cat_id`, `operations`, `name`, `url_prefix`) VALUES ('16', '86', 'approve', 'Verify security', 'non_eligible_security/view'); 

UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `name` = 'All Security Preview' WHERE `id` = '91'; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_1` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_2` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_3` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_4` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_5` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_6` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_7` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_8` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`docs_seg_9` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET utf8 COLLATE utf8_unicode_ci NULL; 


ALTER TABLE `edoc_pbl_v2`.`ch_articles_of_association` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_bia_deeds_of_the_mortgaged_property` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_bonded_warehouse_license` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_certificate_of_incorporation` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_comprehensive_insurance_policy` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_corporate_guarantee` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_erc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_filling_of_mortgage_deed_with_rjsc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_fitness_certificate` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_form_xii` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_free_text` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_ground_rent_payment_receipt` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_memorandum_of_association` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_mortgage_deed_duly_registered_` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_original_certified_copy_of_purchase_deed_` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_original_work_order` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_postdated_cheque` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_resolution` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_security_sharing_agreement` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_tax_token` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_ti2` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_union_porishad_tax_payment_receipt_` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_up_to_date_municipal_tax_payment_receipt_` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_valid_irc_along_with_noc` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 
ALTER TABLE `edoc_pbl_v2`.`ch_valid_trade_license` CHANGE `doc_sts` `doc_sts` ENUM('Obtain','Partially Obtained','Deferral','Inadequacy','Waiver','Pending','Not Applicable','N/A') CHARSET latin1 COLLATE latin1_swedish_ci NULL; 



UPDATE `edoc_pbl_v2`.`child_table_fields` SET `mandatory_sts` = '0' WHERE `id` = '15';


DROP TABLE IF EXISTS `ref_type_of_policy`;

CREATE TABLE `ref_type_of_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sts` tinyint(2) DEFAULT '1',
  `e_by` int(11) DEFAULT NULL,
  `e_dt` datetime DEFAULT NULL,
  `u_by` int(11) DEFAULT NULL,
  `u_dt` datetime DEFAULT NULL,
  `d_by` int(11) DEFAULT NULL,
  `d_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `ref_type_of_policy` */

insert  into `ref_type_of_policy`(`id`,`name`,`sts`,`e_by`,`e_dt`,`u_by`,`u_dt`,`d_by`,`d_dt`) values (1,'Fire & RSD',1,1,'2018-03-29 00:00:00',1,'2018-11-12 00:00:00',NULL,NULL),(2,'Flood & Cyclone',1,1,'2018-03-29 00:00:00',1,'2018-11-12 00:00:00',NULL,NULL),(3,'Earthquake',1,1,'2018-03-29 00:00:00',1,'2018-11-12 00:00:00',NULL,NULL),(4,'Earthquake, Flood & Cyclone',1,1,'2018-03-29 00:00:00',1,'2018-11-12 00:00:00',NULL,NULL),(5,'IAR',1,1,'2018-11-12 00:00:00',NULL,NULL,NULL,NULL),(6,'EEI',1,1,'2018-11-12 00:00:00',NULL,NULL,NULL,NULL),(7,'Fire-RSD, Earthquake, Flood & Cyclone',1,1,'2018-11-12 00:00:00',NULL,NULL,NULL,NULL),(8,'Marine Hull & Machinery (Vessel)',1,1,'2018-11-12 00:00:00',NULL,NULL,NULL,NULL),(9,'Comprehensive Risk (Vehicle)',1,1,'2018-11-12 00:00:00',NULL,NULL,NULL,NULL);


  UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `name` = 'Safe In-Out Temporary' WHERE `id` = '34';

  UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `name` = 'Safe In-Out Permanent' WHERE `id` = '35';
  UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `sts` = '0' WHERE `id` = '87'; 
  UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `sts` = '0' WHERE `id` = '78';
  UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `s_order` = '7' WHERE `id` = '72'; 
  UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `s_order` = '8' WHERE `id` = '73'; 

  UPDATE `edoc_pbl_v2`.`par_docs_title` SET `has_child_sts` = '0' WHERE `id` = '4'; 
  UPDATE `edoc_pbl_v2`.`par_docs_title` SET `file_upload_sts` = '1' WHERE `id` = '4'; 

  ALTER TABLE `edoc_pbl_v2`.`temp_uploaded_file` ADD COLUMN `id` BIGINT(22) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`); 
  UPDATE `edoc_pbl_v2`.`par_docs_title` SET `has_child_sts` = '0' , `file_upload_sts` = '1' WHERE `id` = '56';
  UPDATE `edoc_pbl_v2`.`par_docs_title` SET `has_child_sts` = '0' , `file_upload_sts` = '1' WHERE `id` = '58';
  UPDATE `edoc_pbl_v2`.`par_docs_title` SET `has_child_sts` = '0' , `file_upload_sts` = '1' WHERE `id` = '59';
  UPDATE `edoc_pbl_v2`.`par_docs_title` SET `has_child_sts` = '0' , `file_upload_sts` = '1' WHERE `id` = '60';

 UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `sts` = '0' WHERE `id` = '46'; 
 UPDATE `edoc_pbl_v2`.`sys_link_cat` SET `sts` = '0' WHERE `id` = '83'; 

 ALTER TABLE `edoc_pbl_v2`.`app_customer` CHANGE `accounts_type` `accounts_type` VARCHAR(250) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;

 INSERT INTO `edoc_pbl_v2`.`ref_customer_legal_form` (`name`, `code`, `cib_legal_form_name`) VALUES ('Group', '18', 'Group');


 ALTER TABLE `edoc_pbl_v2`.`app_customer` ADD COLUMN `group_concern` TEXT NULL AFTER `netdt`; 


 ALTER TABLE `edoc_pbl_v2`.`title_level_access` CHANGE `last_updt` `last_updt` DATETIME NOT NULL; 


 TRUNCATE TABLE `edoc_pbl_v2`.`ref_serveyor_name`; 

insert  into `edoc_pbl_v2`.`ref_serveyor_name`(`id`,`name`,`sts`,`e_by`,`e_dt`,`u_by`,`u_dt`,`d_by`,`d_dt`) values (1,'Baltic Control (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Jorip O ParidarshanCompany Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Geodetic Survey Corporation\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Shatabdi Builder\'s & Survey Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(5,'M/S. Eastland Surveyors\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(6,'G. K. Adjusters Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(7,'IIS Consulting (BD) Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(8,'M/S. Unique Service Bureau\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Bangladesh Inspection & Survey (Pvt.) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(10,'M/s. Nandanick Bangladesh\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(11,'IHS Inspection Services (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(12,'MSK Inspection Company Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(13,'M/s. The Engineers Inspection\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(14,'National Survey Bangladesh Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(15,'Lima Claims Surveyor\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(16,'Land Survey & Solutions Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(17,'Commodity Inspection Service (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(18,'M/S. Saybolt Adjusters\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(19,'Mollik Adjusters Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(20,'Appon Inspection Services (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(21,'Promise Surveyors Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(22,'Northern Inspection Co. Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(23,'Prime(BD) Inspection Services Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(24,'Royal Inspection International Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(25,'Digital Inspection & Survey (Pvt.) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(26,'Mridha & Associates Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(27,'Shahporan International Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(28,'Hi-Care Inspection Company\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(29,'Greenland Inspections\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(30,'City Engineering Inspection Co. Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(31,'City Engineering Consultants\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(32,'SF Inspection Company Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(33,'City Survey Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(34,'WIES Consulting (Pvt.) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(35,'Business Survey & Services Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(36,'Zass Survey Limited\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(37,'Padma Techno-Consult Survey Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(38,'Prince Inspection Services (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(39,'E. S. Inspection & Consulting (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(40,'Prime Consulting (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(41,'AST North Bengal Inspection Co. Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(42,'Inspection Engineering & Survey (IES) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(43,'Professional Survey & Engineering Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(44,'M/S. Sohel Associates\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(45,'Tista Surveyor\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(46,'Freedom Inspection Service (BD) Ltd.\r',1,NULL,NULL,NULL,NULL,NULL,NULL),(47,'Matri Survey Inspection Company\r',1,NULL,NULL,NULL,NULL,NULL,NULL);

ALTER TABLE `edoc_pbl_v2`.`app_guarantee` ADD COLUMN `odp_staff_id` VARCHAR(30) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `obl_staff`;

insert into `ref_field_list` (`ref_table_name`, `field_name`, `field_caption`, `field_type`, `list_ref_name`, `list_ref_show`, `list_ref_value`, `data_type`, `data_length`, `mandatory_sts`, `unique_sts`, `sorting_order`, `alignment`, `sts`) values('ref_bank_and_share_issuer','name','Name','1','','','','2','150','1','1','0','Left','1');

DROP TABLE IF EXISTS `ref_bank_and_share_issuer`;

CREATE TABLE `ref_bank_and_share_issuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `sts` tinyint(2) DEFAULT '1',
  `e_by` int(11) DEFAULT NULL,
  `e_dt` datetime DEFAULT NULL,
  `u_by` int(11) DEFAULT NULL,
  `u_dt` datetime DEFAULT NULL,
  `d_by` int(11) DEFAULT NULL,
  `d_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

/*Data for the table `ref_bank_and_share_issuer` */

INSERT INTO `ref_list` (`table_name`, `ref_name`, `ref_remarks`, `sts`) VALUES('ref_location','Location','','1');
INSERT INTO `ref_list` (`table_name`, `ref_name`, `ref_remarks`, `sts`) VALUES('ref_bank_and_share_issuer','Bank And Share Issuer','','1');

insert  into `ref_bank_and_share_issuer`(`id`,`name`,`sts`,`e_by`,`e_dt`,`u_by`,`u_dt`,`d_by`,`d_dt`) values (1,'A B BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(2,'AGRANI BANK LTD',1,NULL,NULL,NULL,NULL,NULL,NULL),(3,'BANK ASIA LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(4,'BANGLADESH BANK',1,NULL,NULL,NULL,NULL,NULL,NULL),(5,'BANGLADESH KRISHI BANK',1,NULL,NULL,NULL,NULL,NULL,NULL),(6,'BRAC BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(7,'COMMERCIAL BANK OF CEYLON LTD',1,NULL,NULL,NULL,NULL,NULL,NULL),(8,'THE CITY BANK LIMITED.',1,NULL,NULL,NULL,NULL,NULL,NULL),(9,'CITIBANK N.A.',1,NULL,NULL,NULL,NULL,NULL,NULL),(10,'DUTCH BANGLA BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(11,'DHAKA BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(12,'EASTERN BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(13,'EXPORT IMPORT BANK OF BANGLADESH LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(14,'FIRST SECURITY BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(15,'HABIB BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(16,'ISLAMI BANK BANGLADESH LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(17,'ICB ISLAMIC BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(18,'ICICI BANK, MUMBAI',1,NULL,NULL,NULL,NULL,NULL,NULL),(19,'IFIC BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(20,'JAMUNA BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(21,'JANATA BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(22,'MASHREQ BANK PSC',1,NULL,NULL,NULL,NULL,NULL,NULL),(23,'MERCANTILE BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(24,'MUTUAL TRUST BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(25,'NATIONAL BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(26,'NATIONAL CREDIT & COMMERCE BANK LTD',1,NULL,NULL,NULL,NULL,NULL,NULL),(27,'ONE BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(28,'PRIME BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(29,'THE PREMIER BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(30,'PUBALI BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(31,'RUPALI BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(32,'STATE BANK OF INDIA',1,NULL,NULL,NULL,NULL,NULL,NULL),(33,'STANDARD BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(34,'STANDARD CHARTERED BANK',1,NULL,NULL,NULL,NULL,NULL,NULL),(35,'SOUTHEAST BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(36,'SHAHJALAL ISLAMI BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(37,'SONALI BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(38,'TRUST BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(39,'UTTARA BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(40,'UNITED COMMERCIAL BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(41,'WOORI BANK',1,NULL,NULL,NULL,NULL,NULL,NULL),(42,'BANK ALFALAH LTD',1,NULL,NULL,NULL,NULL,NULL,NULL),(43,'BASIC BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(44,'COMMERZBANK, AG FRANKFURT',1,NULL,NULL,NULL,NULL,NULL,NULL),(45,'WACHOVIA BANK, NY',1,NULL,NULL,NULL,NULL,NULL,NULL),(46,'AL-ARAFAH ISLAMI BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(47,'The Hong Kong and Shanghai Banking Corporation Ltd.',1,NULL,NULL,NULL,NULL,NULL,NULL),(48,'NATIONAL BANK OF PAKISTAN',1,NULL,NULL,NULL,NULL,NULL,NULL),(49,'BANGLADESH COMMERCE BANK LTD',1,NULL,NULL,NULL,NULL,NULL,NULL),(50,'SOCIAL ISLAMI BANK LTD.',1,NULL,NULL,NULL,NULL,NULL,NULL),(51,'BANGLADESH SHILPA BANK',1,NULL,NULL,NULL,NULL,NULL,NULL),(52,'MODHUMOTI BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(53,'BANGLADESH DEVELOPMENT BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(54,'RAJSHAHI KRISHI UNNAYAN BANK',1,NULL,NULL,NULL,NULL,NULL,NULL),(55,'MEGHNA BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(56,'MIDLAND BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(57,'NRB BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(58,'NRB COMMERCIAL BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(59,'NRB GLOBAL BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(60,'SIMANTO BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(61,'SOUTH BANGLA AGRICULTURE AND COMMERCE BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(62,'THE FARMERS BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(63,'UNION BANK LIMITED',1,NULL,NULL,NULL,NULL,NULL,NULL),(64,'EXIM Bank Limited',1,NULL,NULL,NULL,NULL,NULL,NULL),(65,'Ansar VDP Unnayan Bank',1,NULL,NULL,NULL,NULL,NULL,NULL),(66,'Karmashangosthan Bank',1,NULL,NULL,NULL,NULL,NULL,NULL),(67,'Probashi Kollyan Bank',1,NULL,NULL,NULL,NULL,NULL,NULL),(68,'Grameen Bank',1,NULL,NULL,NULL,NULL,NULL,NULL),(69,'Jubilee Bank',1,NULL,NULL,NULL,NULL,NULL,NULL),(70,'Palli Sanchay Bank',1,NULL,NULL,NULL,NULL,NULL,NULL);

ALTER TABLE `edoc_pbl_v2`.`csi_land_building_flat_gold` ADD COLUMN `shared_with` VARCHAR(255) NULL AFTER `security_shared`;

insert into `ref_field_list` (`ref_table_name`, `field_name`, `field_caption`, `field_type`, `list_ref_name`, `list_ref_show`, `list_ref_value`, `data_type`, `data_length`, `mandatory_sts`, `unique_sts`, `sorting_order`, `alignment`, `sts`) values('ref_location','name','Name','1','','','','2','100','1','0','0','Left','1');
