<script type="text/javascript">
jQuery('#form').jqxValidator({
			rules: [
			{ input: '#sec_id .ms-parent', message: 'Required!', action: 'change, blur', rule: function(input){
					if(jQuery("#security").val() == '' || jQuery("#security").val() == null)
					{return false; }
					return true;
				}
            },
			{ input: '#app_ref_no', message: 'required!', action: 'blur', rule: function(input){
					var item = jQuery("#app_ref_no").jqxComboBox('getSelectedItem');
					if(item != null)
					{
						if(item.value !='')
						{							
							return true;				
						}
						else { return false;}
					}					
					else { return false;}
				}  
			},
			
			{ input: '#cust_ID', message: 'CIF is required!', action: 'keyup, blur', rule: 'required' },
			{ input: '#cust_ID', message: 'Enter Valid CIF!', action: 'keyup, blur', rule: function(input,commit){
					if(input.val()!='')
					{ return alphaNumericCheck(input.val()); }
					else return true;
				}
            },
			{ input: '#cust_ID', message: 'Customer Not Found!', action: 'blur', rule: function (input, commit) {					
					if(input.val()!=='' && alphaNumericCheck(jQuery('#cust_ID').val()) != false)
					{
						jQuery("#sendButton").hide();
						jQuery("#loading").show();
						var flag=0;
						jQuery.ajax({
							type: "POST",
							async: false,
							url: "<?=base_url()?>index.php/cust_doc/getCustomer",
							data : {val: input.val()},
							datatype: "html",
							success: function(response){
									if(response == '1')
									{
										jQuery('#cust_info').html('');
										commit(false);
										flag=1;
									}
									else 
									{
										jQuery('#cust_info').html(response);									
										commit(true);
										flag=0;
									}
								}
							});
						if(flag==1){ return false; }
						else { return true;}	
					}
					else {return true;}
				} 
			}	
			
			]
		});
jQuery("#sendButton").click(function () {		
			var validationResult = function (isValid) {
				if (isValid && OnclikValidation()!=false) {
					call_ajax_submit();
				}
				jQuery("#sendButton").show();
				jQuery("#loading").hide();
			}
			jQuery('#form').jqxValidator('validate', validationResult);
		});
</script>