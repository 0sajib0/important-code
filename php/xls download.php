<?
function inade_doc_xls($search_fieldes=NULL)
	{	
		$aray=explode(":",$search_fieldes);
					
		$customer_id=$aray[0];			
		$dm_id=$aray[1];
		$rm_id=$aray[2];
		$branch_unit_slt=$aray[3];
		$custname=$aray[4];
		
		$result_inad = $this->doc_report_model->get_Inadicuices_xl($customer_id,1,$branch_unit_slt,$dm_id,$rm_id,$custname);
		
		error_reporting(E_ALL);
		date_default_timezone_set('Asia/Dhaka');		
		include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);			
		$rowNumber = 1;	
		
		//$title_name=' Details';
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber, 'Document Inadequacy Report');
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$rowNumber.':K'.$rowNumber);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber)->getFont()->setBold(true);
		
		$rowNumber++;$rowNumber++;
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60); 
		
		foreach(range('A','E') as $columnID) // to set auto column widh
		{
		    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		foreach(range('G','K') as $columnID) // to set auto column widh
		{
		    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		
		$headings2 = array('SL','Branch','CIF No.','Customer Name','Doc Catagory','Doc Title','Approval Reference No.','Regularization date','RM Name','DM Name','Remarks');
		$seg=1;
		$objPHPExcel->getActiveSheet()->fromArray(array($headings2),NULL,'A'.$rowNumber);				
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getFont()->setBold(true);
		$rowNumber++;				
	
		$counter=1;	
					
		foreach($result_inad as $row1)
		{
			if($row1->doc_sts == 'Inadequacy')
			{	
				$headings2 = array($counter,$row1->branchname,$row1->cust_full_id,htmlspecialchars_decode($row1->custname),$row1->headname,$row1->titlename,$row1->app_ref_no,$row1->reg_date,$row1->rmname,$row1->dmname,$row1->remarks);
				$objPHPExcel->getActiveSheet()->fromArray(array($headings2),NULL,'A'.$rowNumber);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$rowNumber, $row1->cust_full_id, PHPExcel_Cell_DataType::TYPE_STRING);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getAlignment()->setWrapText(true);
							
				$rowNumber++;																	
				$counter++;
			}
			elseif($row1->doc_sts == 'N/A' && $row1->child_table_name!='')
			{
				$q1 = $this->db->query("SELECT s1.* FROM ".$row1->child_table_name." s1 
				WHERE s1.docs_id = ".$row1->id." AND s1.doc_sts='Inadequacy' AND s1.sts=1")->result();
				foreach ($q1 as $row)
				{
					if($row->doc_sts == 'Inadequacy')
					{
						$headings2 = array($counter,$row1->branchname,$row1->cust_full_id,htmlspecialchars_decode($row1->custname),$row1->headname,$row1->titlename,$row1->app_ref_no,$row1->reg_date,$row1->rmname,$row1->dmname,$row1->remarks);
						$objPHPExcel->getActiveSheet()->fromArray(array($headings2),NULL,'A'.$rowNumber);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$rowNumber.':K'.$rowNumber)->getAlignment()->setWrapText(true);
						$rowNumber++;																	
						$counter++;
					}
				}
			}	
		}
			
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Document Inadequacy Report');			
		
		include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel'.DIRECTORY_SEPARATOR.'IOFactory.php');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');//Excel2007
		header('Content-Type: application/vnd.ms-excel');  
		header('Content-Disposition: attachment;filename="Document_Inadequacy_Report.xls"'); //.xlsx  
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');   
		exit();
	}