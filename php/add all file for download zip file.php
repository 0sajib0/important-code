<?
            $this->load->library('zip');
            $this->load->helper('file');
            $path ='uploads/trademark/tm1/';

            if($query->regulation_copy!=''){
                $this->zip->read_file($path.'regulation_copy/'.$query->regulation_copy,FALSE);
            }
            if($query->affidavit_copy!=''){
                $this->zip->read_file($path.'affidavit_copy/'.$query->affidavit_copy,FALSE);
            }
            if($query->replica_of_mark!=''){
                $this->zip->read_file($path.'markreplica/'.$query->replica_of_mark,FALSE);
            }
            if($query->arranged_replica_copies!=''){
                $arr_arranged_replica_copies = explode('###', $query->arranged_replica_copies);
                for($i=0;$i<count($arr_arranged_replica_copies);$i++){
                    $this->zip->read_file($path.'arranged_replica_copy/'.$arr_arranged_replica_copies[$i],FALSE);
                } 
            }
            if($query->not_bn_en_lang_translation_attested_copy!=''){
                $this->zip->read_file($path.'attested_translation/'.$query->not_bn_en_lang_translation_attested_copy,FALSE);
            }
            if($query->not_bn_en_lang_transliteration_attested_copy!=''){
                $this->zip->read_file($path.'attested_transliteration/'.$query->not_bn_en_lang_transliteration_attested_copy,FALSE);
            }
            if($query->previous_application_data!=''){
                $this->zip->read_file($path.'previous_app_data_files/'.$query->previous_application_data,FALSE);
            }
            if($query->is_all_product_or_service_of_pre_app_with_cur_app_attachment!=''){
                $this->zip->read_file($path.'previous_app_files_for_priority/'.$query->is_all_product_or_service_of_pre_app_with_cur_app_attachment,FALSE);
            }
            if($query->applicant_signature!=''){
                $this->zip->read_file($path.'signature/'.$query->applicant_signature,FALSE);
            }
            if($query->payment_scan_copy!=''){
                $this->zip->read_file($path.'payment_scan_copy/'.$query->payment_scan_copy,FALSE);
            }
            if($query->other_attachments!=''){
                $arr_other_attachments = explode('###', $query->other_attachments);
                for($i=0;$i<count($arr_other_attachments);$i++){
                    $this->zip->read_file($path.'other_attachments/'.$arr_other_attachments[$i],FALSE);
                } 
            }

            if($file1!=''){
                $this->zip->read_file($path.$file1,FALSE);
            }
            if($file2!=''){
                $this->zip->read_file($path.$file2,FALSE);
            }
            $this->zip->read_file($file,FALSE);
            $this->zip->download("all_patentube_files.zip");












?>