<?php

function convert_number($number) 
{ 
    if (($number < 0) || ($number > 99999999999999)) 
    { 
        return "$number"; 
    } 
	
	$Co = floor($number / 10000000);  /* Crore (giga) */ 
    $number -= $Co * 10000000;
	
  /*  $Gn = floor($number / 1000000);  
    $number -= $Gn * 1000000;*/
	
	$lukn = floor($number / 100000);  //luk (giga) 
    $number -= $lukn * 100000;
	 
    $kn = floor($number / 1000);     /* Thousands (kilo) */ 
    $number -= $kn * 1000; 
    $Hn = floor($number / 100);      /* Hundreds (hecto) */ 
    $number -= $Hn * 100; 
    $Dn = floor($number / 10);       /* Tens (deca) */ 
    $n = $number % 10;               /* Ones */ 

    $res = ""; 

    if ($Co) 
    { 
        $res .= convert_number($Co) . " Crore"; 
    } 
	/*
	if ($Gn) 
    { 
        $res .= convert_number($Gn) . " Million"; 
    }*/ 
	if ($lukn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($lukn) . " Lac"; 
    } 

    if ($kn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($kn) . " Thousand"; 
    } 

    if ($Hn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($Hn) . " Hundred"; 
    } 

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
        "Nineteen"); 
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
        "Seventy", "Eigthy", "Ninety"); 

    if ($Dn || $n) 
    { 
        if (!empty($res)) 
        { 
            $res .= " and "; 
        } 

        if ($Dn < 2) 
        { 
            $res .= $ones[$Dn * 10 + $n]; 
        } 
        else 
        { 
            $res .= $tens[$Dn]; 

            if ($n) 
            { 
                $res .= "-" . $ones[$n]; 
            } 
        } 
    } 

    if (empty($res)) 
    { 
        $res = "zero"; 
    } 
	
    return $res; 
	}	
function convet_TK($amountcon)
{
	$NO_A=explode(".",$amountcon);
	if (count($NO_A)==1){$part1=$NO_A[0]; $part2=0;
	return 'Taka '.convert_number($part1).' only';
	}else if (count($NO_A)==2 && ($NO_A[1]==0 || $NO_A[1]=='00')){$part1=$NO_A[0]; $part2=$NO_A[1];
	return 'Taka '.convert_number($part1).' only';
	}else{$part1=$NO_A[0]; $part2=$NO_A[1];
	return 'Taka '.convert_number($part1).' and Paisa '.convert_number($part2).' only';
	}
}	
function coma2($amout,$len2)
{
	$out2r=$amout;
	$out22r='';
	$len2h=$len2;
	for($i=1; $i<$len2; $i=$i+2)
	{				
		$outNext='';
		$out22r=substr($out2r, $len2h-2).','.$out22r;			
		$outNext=substr($out2r, 0, $len2h-2);
		if(strlen($outNext)<=2){
		 $out22r=$outNext.','.$out22r;
		 break;
		}
		$len2h=strlen($outNext);				
		$out2r=$outNext;		
	
	}
	return $out22r;
}
function comma($amout)
{
	$minSign='';
	//$removeDot[1]='';
	$len3=0;
	if($amout<0){	
		$removeminus=str_replace("-","",$amout);
		$amout=$removeminus;
		$minSign='-';
	}
	$removeDot=explode(".",$amout);
	if (count($removeDot)==1){$amountf=$amout; $spart='00';}else{$amountf=$removeDot[0]; $spart=$removeDot[1];}	
	//if($removeDot[1]==''){$spart='00';}else{$spart=$removeDot[1];}	
	$len3=strlen($amountf);
	if($len3 > 3)
	{
		$out3=substr($amountf, $len3-3);
		$out2=substr($amountf, 0, $len3-3);
		$len2=strlen($out2);
		$out2r=$out2;
		
		if($len2 > 2){
			$out22=coma2($out2,$len2);
			$result=$out22.$out3.'.'.$spart;		
		}
		else{
		$result=$out2.','.$out3.'.'.$spart;
		}		
	}
	else
	{
	$result=$removeDot[0].'.'.$spart;
	}
	return $minSign.$result;
}




?>